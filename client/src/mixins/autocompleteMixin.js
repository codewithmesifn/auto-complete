import axios from "axios";

const url = "http://localhost:3000/api/regions";

export const autocompleteMixin = {
  data: () => ({
    items: [],
    model: null,
    search: null,
    textMsg: '',
    show: true,
    showBtn: false,
    regionName: '',
    isModalVisible: false,
    phone:'',
    phoneErr:'',
    r_name:''
  }),
  watch: {
    search(val) {
      this.show = false;
      this.showBtn = true;
      this.textMsg = "A region with this name is not registered yet!";
      axios
        .get(url)
        .then(res => {
          this.items = res.data;
          if (this.items.length > 0) {
            return;
          }
        })
        .catch(error => {
          console.log(error);
        })
        .finally(() => (this.isLoading = false));
    }
  },

  methods: {
    showAdd() {
      this.dialog = true;
    },
    save(event) {
      this.phone = event;
    },
    saveRegion() {
      if (this.phone != "") {
        const regionData = {
          name: this.regionName,
          phone: this.phone
        };
        axios.post(url, regionData).then(() => {
          alert("Region Added!");
          this.$router.go();
          this.closeModal();
        });
      }
    },
    showModal() {
      this.isModalVisible = true;
      this.regionName = this.model;
    },
    closeModal() {
      this.isModalVisible = false;
    },
  }
};
