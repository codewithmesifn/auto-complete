import axios from "axios";

const url = "http://localhost:3000/api/regions";

export const otherAutocompleteMixin = {
  data() {
    return {
      loading: false,
      items: [],
      search: null,
      select: null,
      states: [],
      txtMsg: "A region with this name is not registered yet.",
      beforeKey: true,
      noData: false,
      isModalVisible: false
    };
  },
  watch: {
    search(val) {
      val && val !== this.select && this.querySelections(val);
    }
  },
  methods: {
    save(event) {
      this.phone = event;
    },
    saveRegion() {
      if (this.phone != "") {
        const regionData = {
          name: this.regionName,
          phone: this.phone
        };
        axios.post(url, regionData).then(() => {
          alert("Region Added!");
          this.$router.go();
          this.closeModal();
        });
      }
    },
    querySelections(v) {
      this.loading = true;
      setTimeout(() => {
        this.beforeKey = false;
        this.noData = true;
      }, 500);
      // Simulated ajax query
      axios
        .get(url)
        .then(res => {
          for (let i = 0; i < res.data.length; i++) {
            this.states.push(res.data[i].name);
          }
        })
        .catch(err => {});
      setTimeout(() => {
        this.items = this.states.filter(e => {
          return (e || "").toLowerCase().indexOf((v || "").toLowerCase()) > -1;
        });
        this.loading = false;
      }, 500);
    },
    showModal() {
      this.isModalVisible = true;
      this.regionName = this.model;
    },
    closeModal() {
      this.isModalVisible = false;
    }
  }
};
