/* eslint-disable */

import Vue from "vue";
import App from "./App.vue";
import Vuetify from "vuetify";

import "vuetify/dist/vuetify.min.css";
import "material-design-icons-iconfont/dist/material-design-icons.css";

import VueRouter from "vue-router";

import { routes } from "./router/index";

Vue.use(VueRouter);

import VueSweetalert2 from "vue-sweetalert2";

Vue.use(VueSweetalert2);

const router = new VueRouter({
  mode: "history",
  routes
});

Vue.use(Vuetify);

Vue.config.productionTip = false;

new Vue({
  el: "#app",
  router,
  render: h => h(App)
});
